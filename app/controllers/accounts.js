'use strict'

const User = require('../models/user');
const Blog = require('../models/blog');
const Joi = require('joi');

//handler for main hbs, replying with view
exports.main = {
  auth: false,
  handler: (request, reply) => {
    reply.view('main');
  },

};

//handler for logout route
exports.logout = {
  auth: false,
  handler: function (request, reply) {
    request.cookieAuth.clear();
    reply.redirect('/');
  },
};

//handler for signup route, rendering signup.hbs
exports.signup = {
  auth: false,
  handler: function (request, reply) {
    reply.view('signup');
  },

};

//handler replying with login.hbs
exports.login = {
  auth: false,
  handler: function (request, reply) {
    reply.view('login');
  },

};

//handler for register post route, validating form has been filled correctly
//and creating a new user if so, saving to database
exports.register = {
  auth: false,

  validate: {

    payload: {
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },

    failAction: function (request, reply, source, error) {
      reply.view('signup', {
        title: 'Sign up error',
        errors: error.data.details,
      }).code(400);
    },

    options: {
      abortEarly: false,
    },

  },

  handler: function (request, reply) {
    User.findOne({ email: request.payload.email }).then(existingUser => {
    if(existingUser === null) {
      const user = new User(request.payload);
      user.save().then(newUser => {
        reply.redirect('/login');
      });
    } else {
      let message = "Email already exists."
      reply.view('signup', { existingUser: message });
    }
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

//hanlder for authenticate post route, allowing for admin sign in or otherwise
//checking input credentials against users in the database, if found replying with their home page
//if not, replying with signup.hbs
exports.authenticate = {
  auth: false,

  validate: {

    payload: {
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },

    failAction: function (request, reply, source, error) {
      reply.view('login', {
        title: 'Sign up error',
        errors: error.data.details,
      }).code(400);
    },

    options: {
      abortEarly: false,
    },

  },

  handler: function (request, reply) {
    const user = request.payload;
    User.findOne({ email: user.email }).then(foundUser => {
      if (user.email === 'admin@admin.com' && user.password === 'secret') {
        request.cookieAuth.set({
          loggedIn: true,
          loggedInUser: 'admin@admin.com',
        });
        reply.redirect('/admin');
      }else if (foundUser && foundUser.password === user.password) {
        request.cookieAuth.set({
          loggedIn: true,
          loggedInUser: user.email,
        });
        reply.redirect('/postblog');
      } else {
        reply.redirect('/signup');
      }
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

//handler for viewsettings route, finding the logged in user and rendering their details to the
// page
exports.viewSettings = {

  handler: function (request, reply) {
    var userEmail = request.auth.credentials.loggedInUser;
    User.findOne({ email: userEmail }).then(foundUser => {
      Blog.find({ user: foundUser._id }).then(userBlogs => {
        let blogslength = userBlogs.length;
        reply.view('settings', {title: 'Edit Account Settings', user: foundUser, numberBlogs: blogslength});
      });
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

//handler for updatesettings route, validating the form and making specified changes
exports.updateSettings = {

  validate: {

    payload: {
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },

    failAction: function (request, reply, source, error) {
      reply.view('settings', {
        title: 'Sign up error',
        errors: error.data.details,
      }).code(400);
    },

    options: {
      abortEarly: false,
    },
  },
  handler: function (request, reply) {
    const update = request.payload;
    var userEmail = request.auth.credentials.loggedInUser;
    User.findOne({ email: userEmail }).then(user => {
      if (update.password === user.password) {
        user.email = (update.email === '') ? userEmail.email : update.email;
        user.firstName = (update.firstName === '') ? userEmail.firstName : update.firstName;
        user.lastName = (update.lastName === '') ? userEmail.lastName : update.lastName;
      }

      return user.save();
    }).then(user => {
      reply.view('settings', { title: 'Edit Account Settings', user: user });
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

//handler for upload image route, taking in payload from form, getting the logged in user
//finding by email, and initializing the users img field with appropriate data
exports.uploadImage = {

  handler: function (request, reply) {
    const data = request.payload.picture;
    var userEmail = request.auth.credentials.loggedInUser;
    User.findOne({ email: userEmail }).then(user => {
      user.img.data = data;
      user.img.contentType = String;
      return user.save();
    }).then(user => {
      reply.view('settings', { user: user });
    }).catch(err => {
      reply.redirect('settings');
    });
  },

};

//handler for getimage route, taking passed param from page, finding the user , 
//and replying with their image
exports.getImage = {

  handler: function (request, reply) {
    User.findOne({ _id: request.params._id }).then(user => {
      reply(user.img.data).type('image');
    });
  },

};

