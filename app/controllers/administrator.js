'use strict'

const User = require('../models/user');
const Blog = require('../models/blog');
const AdminPage = require('../views/admin');

//handler for admin route, rendering admin home page, posting users in filter dropdown
//all users in delete dropdown, all blogs in table
exports.admin = {

  handler: function (request, reply) {
    let postingUser = [];
    let allUsers = [];
    Blog.find({}).populate('user').then(allBlogs => {
      for (let us of allBlogs) {
        postingUser.push(us.user);
      }

      const uniqueUsers = postingUser.filter(function (elem, index, self) {
        return index == self.indexOf(elem);
      });

      User.find({}).then(users => {
        for (let user of users) {
          allUsers.push(user);
        }
        allBlogs.sort({datefield: -1});
        return allUsers;
      }).then(allUsers => {
        reply.view('admin', {
          title: 'User Blogs',
          blogs: allBlogs,
          user: uniqueUsers,
          allUsers: allUsers,
        });
      });
    }).catch(err => {
      reply.redirect('/');
    });
  },
};

//This was previously the method i was using to filter blogs, but swapped out for jquery
//which was much more effective for the approach i had taken with the admin page.
//left here to show implementation
/*exports.selectUserBlogs = {

  handler: function (request, reply) {
    const data = request.payload;
    if (data.email === '' || data.email === 'all bloggers') {
      console.log('hitting here1');
      reply.redirect('admin');
    }else {
      User.findOne({ email: data.email }).then(selectedUser => {
        return Blog.find({ user: selectedUser }).populate('user').then(userBlogs => {
          let postingUser = [];
          Blog.find({}).populate('user').then(blogs => {
            for (let us of blogs) {
              postingUser.push(us.user);
            }

            return postingUser;
          }).then(postingUser => {
            const uniqueUsers = postingUser.filter(function (elem, index, self) {
              return index == self.indexOf(elem);
            });

            let allUsers = [];
            User.find({}).then(users => {
              for (let user of users) {
                allUsers.push(user);
              }
            });

            reply.view('admin', {
              title: 'User Blogs',
              blogs: userBlogs,
              user: uniqueUsers,
              allUsers: allUsers,
            });
          });
        });
      }).catch(err => {
        reply.redirect('admin');
      });
    }
  },
};*/

//handler for deleting blogs from admin page, dealing with individual, array or empty entry.
exports.deleteUserSelectedBlogs = {
  handler: function (request, reply) {
    const data = request.payload;
    if (data !== undefined) {
      if (Array.isArray(data._id)) {
        const range = data._id.length;
        for (let i = 0; i < range; i++) {
          Blog.findOne({ _id: data._id[i] }).populate('user').then(deldata => {
            return Blog.remove(deldata);
          }).catch(err => {
            reply.redirect('admin');
          });
        }
      } else {
        Blog.findOne({ _id: data._id }).then(deldata => {
          return Blog.remove(deldata);
        }).catch(err => {
          reply.redirect('admin');
        });
      }
    }

    reply.redirect('admin');
  },
};

//handler for deleting selected user, finding the user, removing the users blogs and removing user.
exports.deleteUser = {

  handler: function (request, reply) {
    const data = request.payload;
    const userDelete = data.email;
    User.findOne({ email: data.email }).populate('blogs').then(user => {
      Blog.find({ user: user._id }).then(blogs => {
        return Blog.remove({ user: user._id });
      });
      User.findOne({ _id: user._id }).then(user=> {
        return User.remove(user);
      });
      reply.redirect('admin');
    }).catch(err => {
      reply.redirect('admin');
    });
  },
};

//handler for adminregister route, allowing admin to create users without leaving his view.
exports.adminRegister = {
  handler: function (request, reply, err) {
    User.findOne({ email: request.payload.email }).then(existingUser => {
      if(existingUser === null) {
        const user = new User(request.payload);
        user.save().then(newUser => {
          reply.redirect('/admin');
        });
      } else {
        throw err;
      }
    }).catch(err => {
      //I am aware that this causes a Error, Uncaught. But it is serving my purpose of triggering 
      //ajax error function.
      throw err;
    });
  },

};

