'use strict'
const User = require('../models/user');
const Blog = require('../models/blog');

//handler for public blog page, getting all blogs, rendering only posting users to dropdown
exports.publicblogs = {

  handler: function (request, reply) {
    let postingUser = [];
    let uniqueUsers = [];
    Blog.find({}).populate('user').then(allBlogs => {
      for (let us of allBlogs) {
        postingUser.push(us.user);
      }

      uniqueUsers = postingUser.filter(function (elem, index, self) {
        return index == self.indexOf(elem);
      });
      allBlogs.sort({datefield: -1});
      reply.view('publicblogs', {
        title: 'Public Blogs',
        blogs: allBlogs,
        user: uniqueUsers,
      });
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

//handler for select blog route, rendering specified user posts only along with dropdown only
//populated with posting users.
exports.selectBlogger = {

  handler: function (request, reply) {
    const data = request.payload;
    if (data.user === '' || data.user === 'all bloggers') {
      reply.redirect('publicblogs');
    }else {
      User.findOne({ email: data.user }).then(selectedUser => {
        return Blog.find({ user: selectedUser }).populate('user').then(userBlogs => {
          let postingUser = [];
          Blog.find({}).populate('user').then(blogs => {
            for (let us of blogs) {
              postingUser.push(us.user);
            }

            return postingUser;
          }).then(postingUser => {
            const uniqueUsers = postingUser.filter(function (elem, index, self) {
              return index == self.indexOf(elem);
            });
            userBlogs.sort({datefield: -1});
            reply.view('publicblogs', {
              title: 'Public Blogs',
              blogs: userBlogs,
              user: uniqueUsers,
            });
          });
        });
      }).catch(err => {
        reply.redirect('publicblogs');
      });
    }
  },
};

