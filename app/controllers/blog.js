'use strict'

const User = require('../models/user');
const Blog = require('../models/blog');

//Handler for user home page, rendering their list of blogs if any
exports.postblog = {
  handler: function (request, reply) {
    const userEmail = request.auth.credentials.loggedInUser;
    User.findOne({ email: userEmail }).then(gotUser => {
      return Blog.find({ user: gotUser._id });
    }).then(userBlogs => {
      reply.view('postblog', { blogs: userBlogs });
    }).catch(err => {
      reply.redirect('postblog');
    });
  },

};

//handler for postblog route, adding user to blog field and  saving to database
exports.postBlog = {
  handler: function (request, reply) {
    const blogData = request.payload;
    const picture = request.payload.img;
    const userEmail = request.auth.credentials.loggedInUser;
    User.findOne({ email: userEmail }).then(user => {
      blogData.user = user._id;
      const blog = new Blog(blogData);
      if (picture.buffer !== undefined) {
        blog.img.data = picture;
        blog.img.contentType = String;
      }
      return blog.save();
    }).then(newBlog => {
        reply.redirect('postblog');
      }).catch(err => {
      reply.redirect('postblog');
    });
  },

};

//handler for delete blog route, deleting selected blogs handling whether it is an individual
//blog or an array, or if selection is empty
exports.deleteSelectedBlogs = {
  handler: function (request, reply) {
    const data = request.payload;
    if (data !== undefined) {
      if (Array.isArray(data._id)) {
        const range = data._id.length;
        for (let i = 0; i < range; i++) {
          Blog.findOne({ _id: data._id[i] }).then(deldata => {
            return Blog.remove(deldata);
          }).catch(err => {
            reply.redirect('postblog');
          });
        }
      } else {
        Blog.findOneAndRemove({ _id: data._id }).then(deldata => {
          return Blog.remove(deldata);
        }).catch(err => {
          reply.redirect('postblog');
        });
      }
    }

    reply.redirect('postblog');
  },

};


//handler for getimage route, taking passed param from page, finding the user , 
//and replying with their image
exports.getBlogImage = {

  handler: function (request, reply) {
    Blog.findOne({ _id: request.params._id }).then(blog => {
      reply(blog.img.data).type('image');
    });
  },

};

