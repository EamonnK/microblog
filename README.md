This app allows users to create accounts, post messages, attach pictures to messages, upload profile pictures and view the blogs of any other user by filter.
Admin account provides a one page live view, dropdowns for delete users and filter user posts, register users on page and a delete posts by checkbox with a select all option.

I have commented out selectuserblogs in administrator.js to show i had implemented it with ajax, but went for jquery for efficiency with the approach i took.

Furthermore, no duplicate registered emails allowed in user or admin registering.

*users and posts seeded at microblog06 tag. commented out there after and connected to mlab, to persist pictures.